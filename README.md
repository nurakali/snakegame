# SnakeGame
Projekt do předmětu B6B36PCC - Programování v C/C++
Autor: Alisher Nurakhmetov (nurakali)
## Popis
Klasicka hra snake. Mate pod kontrolou Hada, ktery pohybuje po mape a ji jablka(ve mem pripade food) aby zvetsit svou delku. Hra skonci, kdyzh se narazis do sveho ocasu.
## Snake Game prirucka
Pro pisteni hry slouzi build "game"

Hra snake ma moc jednoduche ovladani
smerovani Hada:

w <- nahoru

d <- do leva

s <- dolu

a <- do prava

x <- ukoncheni

DULEZITE: vsechny comandy nutne psat do termenalu mezi kazde iterace vykreslovani. Po kazdemu inputu nutne macknout ENTER

## Pouzite knihovny
Pouzhival jsem jenom knihovnu Google Tests.
Pro pusteni testu slozi build "tests"

## Struktira Programu
Projkt ma 3 cpp souboru:

1.Main <- Prusti samotnou hru

2.Game <- Obsahuje logiku hry (Vykreslovani, Input, Logika a take Pripraveni hry ke zacatecnemu stavu)

3.Tests <- Obsahuje testy

## Checklist pro odevzdání


Kód

Obsahuje váš kód CMakeLists.txt přes který se vaše semestrálka dá postavit? Ano

Je implementována nápověda (argument programu --help)? Ne



Použití vláken pro vyšší bodové hodnocení

Používá váš kód alespoň tři vlákna, která správně komunikují? Ano

Nepoužívá váš kód rozšíření jazyka? (Například OpenMP, VLA) Ne

Nepoužívá váš kód nepřenosné knihovny (Například POSIX, Win32, filesystem) Ne



Testování

Obsahuje vaše řešení popis způsobů a postupů testování? Obsahuje příklady testů, které jste prováděli? Ano

Zkontrolovali jste svoje řešení s využitím vhodného analytického nástroje (valgrind, dr. Memory apod.)? Ano



Zpráva

Obsahuje vaše zpráva hash commitu (nebo tag), vůči kterému byla napsaná? Ne, psal jsem na lokalu

Obsahuje vaše zpráva popis zadání? Ano

Obsahuje vaše zpráva popis ovládání hry? Ano

Obsahuje vaše zpráva popis možností a způsobů nastavení hry? Takové možnosti nejsou

Máte proměnné a funkce programu řádně zdokumentovány? Jsou všude doplněny komentáře? Ano