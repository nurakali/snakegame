#include <iostream>
#include <vector>
#include <thread>
#include <condition_variable>
#include <chrono>
// Define game grid size, snake, food, and other game variables
const int width = 10;
const int height = 10;
std::vector<std::pair<int, int>> snake;
std::pair<int, int> food;
enum Direction { STOP = 0, LEFT, RIGHT, UP, DOWN };
Direction dir;
bool gameOver;
std::mutex mtx;
std::condition_variable cv;

// Function declarations
void PrepareGame();
void Draw();
void Input();
void Logic();
void GenerateFood();


void PrepareGame() {
    gameOver = false;
    dir = STOP;
    snake = {{width / 2, (height / 2)}}; // Initial snake position
    GenerateFood(); // Place initial food
}

void Draw() {
    //std::system("cls")/std::system("clear"); // Clear the screen
#ifdef _WIN32
    std::system("cls"); // For Windows
#else
    std::system("clear"); // For Unix-like systems (Linux, macOS)
#endif
    //Drawing upside wall
    for (int i = 0; i < width + 2; i++)
        std::cout << "-#-";
    std::cout << std::endl;

    //Drawing leftside, rightside walls, snake and Food
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            if (j == 0)
                std::cout << " # "; // Left wall

            if (i == food.second && j == food.first)
                std::cout << " F "; // Food
            else if (i == snake[0].second && j == snake[0].first)
                std::cout << " O "; // Head of snake
            else {
                bool print = false;
                for (int k = 1; k < snake.size(); k++) {
                    if (snake[k].first == j && snake[k].second == i) {
                        std::cout << " o "; // Body of snake
                        print = true;
                        break;
                    }
                }
                if (!print)
                    std::cout << "   ";
            }

            if (j == width - 1)
                std::cout << " # "; // Right wall
        }
        std::cout << std::endl;
    }

    for (int i = 0; i < width + 2; i++)
        std::cout << "-#-";
    std::cout << std::endl;
}

void Input() {
    char ch;
    while (!gameOver) {
        std::cin >> ch;
        switch (ch) {
            case 'a':
                if(dir == RIGHT && snake.size() > 1){
                    break;
                }
                dir = LEFT;
                break;
            case 'd':
                if(dir == LEFT && snake.size() > 1){
                    break;
                }
                dir = RIGHT;
                break;
            case 'w':
                if(dir == DOWN && snake.size() > 1){
                    break;
                }
                dir = UP;
                break;
            case 's':
                if(dir == UP && snake.size() > 1){
                    break;
                }
                dir = DOWN;
                break;
            case 'x':
                gameOver = true;
                break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void Logic() {
    while (!gameOver) {
        {
            std::lock_guard<std::mutex> lock(mtx);
            // Move the snake
            std::pair<int, int> prev = snake[0];
            std::pair<int, int> prev2;
            switch (dir) {
                case LEFT:
                    snake[0].first--;
                    break;
                case RIGHT:
                    snake[0].first++;
                    break;
                case UP:
                    snake[0].second--;
                    break;
                case DOWN:
                    snake[0].second++;
                    break;
                default:
                    break;
            }
            for (int i = 1; i < snake.size(); i++) {
                prev2 = snake[i];
                snake[i] = prev;
                prev = prev2;
            }

            // Check collision with food
            if (snake[0].first == food.first && snake[0].second == food.second) {
                snake.push_back(prev);
                GenerateFood();
            }

            // Check collision with walls
            if (snake[0].first >= width) snake[0].first = 0; else if (snake[0].first < 0) snake[0].first = width - 1;
            if (snake[0].second >= height) snake[0].second = 0; else if (snake[0].second < 0) snake[0].second = height - 1;

            // Check collision with self
            for (int i = 1; i < snake.size(); i++) {
                if (snake[i] == snake[0])
                    gameOver = true;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // Control the speed of the snake
    }
}

void GenerateFood() {
    food.first = rand()
                 % width;
    food.second = rand() % height;
// Ensure the food is not generated on the snake
    for (auto& segment : snake) {
        if (segment == food) {
            GenerateFood();
            return;
        }
    }
}