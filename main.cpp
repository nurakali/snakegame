#include <iostream>
#include <thread>
#include <chrono>
#include "Game.cpp"
// Define game grid size, snake, food, and other game variables

int main() {
    PrepareGame();
    std::thread inputThread(Input);
    std::thread logicThread(Logic);

    while (!gameOver) {
        Draw();
        //Input();
        std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // Game speed
    }

    inputThread.join();
    logicThread.join();

    std::cout << "Game Over!" << std::endl;
    return 0;
}
