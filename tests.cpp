//
// Created by Alisher Nurakhmetov on 13.01.2024.
//
#include <gtest/gtest.h>
#include "Game.cpp" // Assuming your game logic is in this file

class SnakeGameTest : public ::testing::Test {
protected:
    void SetUp() override{
        PrepareGame();
    }
};

TEST_F(SnakeGameTest, InitialSetup) {
// Check initial snake position
EXPECT_EQ(snake.size(), 1);
EXPECT_EQ(snake[0], std::make_pair(width / 2, height / 2));

// Check initial direction
EXPECT_EQ(dir, STOP);

// Check initial food position
EXPECT_NE(food, std::make_pair(-1, -1)); // Assuming -1, -1 is an invalid position
}

TEST_F(SnakeGameTest, testOfEatingFood){
    int first = width / 2;
    int second = height / 2;
    food.first = first;
    food.second = second;
    Logic();
    //check if food change position
    EXPECT_NE(food, std::make_pair(first, second));
    //check if snake grow
    EXPECT_EQ(snake.size(), 2);
}

TEST_F(SnakeGameTest, testOfGameEnd){
    //creating snake with collision is first and last point
    snake = {{5,5},
             {4,5},
             {4,4},
             {5,4},
             {5,5}
    };

    //Call Logic function
    Logic();
    EXPECT_EQ(gameOver, true);
}
